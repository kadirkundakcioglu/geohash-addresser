#!/bin/bash

# Check virtual environment
[ ! -d ./venv ] && ./create-venv.sh

# Check data directory
[ ! -d ./data ] && mkdir ./data
[ ! -d ./data/log ] && mkdir ./data/log
[ ! -d ./data/error ] && mkdir ./data/error

# Activate virtual environment
source venv/bin/activate
# Install dependencies
pip -q install -r ./config/deps.list

python app.py $@
while [ $? -ne 0 ]
do
  python app.py $@
done