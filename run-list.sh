#!/bin/bash

DATA_WOF="./data/wof"
DATA_OUT="./data/out"
[ -d $DATA_WOF ] || mkdir -p $DATA_WOF
[ -d $DATA_OUT ] || mkdir -p $DATA_OUT

./rerun.sh -o $DATA_OUT/borough.csv       -d $DATA_WOF/whosonfirst-data-borough-latest -c borough_id -t 6
./rerun.sh -o $DATA_OUT/campus.csv        -d $DATA_WOF/whosonfirst-data-campus-latest -c campus_id -t 6
./rerun.sh -o $DATA_OUT/continent.csv     -d $DATA_WOF/whosonfirst-data-continent-latest -c continent_id -t 6
./rerun.sh -o $DATA_OUT/country.csv       -d $DATA_WOF/whosonfirst-data-country-latest -c country_id -t 6
./rerun.sh -o $DATA_OUT/county.csv        -d $DATA_WOF/whosonfirst-data-county-latest -c county_id -t 6
./rerun.sh -o $DATA_OUT/dependency.csv    -d $DATA_WOF/whosonfirst-data-dependency-latest -c dependency_id -t 6
./rerun.sh -o $DATA_OUT/disputed.csv      -d $DATA_WOF/whosonfirst-data-disputed-latest -c disputed_id -t 6
./rerun.sh -o $DATA_OUT/empire.csv        -d $DATA_WOF/whosonfirst-data-empire-latest -c empire_id -t 6
./rerun.sh -o $DATA_OUT/localadmin.csv    -d $DATA_WOF/whosonfirst-data-localadmin-latest -c localadmin_id -t 6
./rerun.sh -o $DATA_OUT/locality.csv      -d $DATA_WOF/whosonfirst-data-locality-latest -c locality_id -t 6
./rerun.sh -o $DATA_OUT/macrocounty.csv   -d $DATA_WOF/whosonfirst-data-macrocounty-latest -c macrocounty_id -t 6
./rerun.sh -o $DATA_OUT/macrohood.csv     -d $DATA_WOF/whosonfirst-data-macrohood-latest -c macrohood_id -t 6
./rerun.sh -o $DATA_OUT/macroregion.csv   -d $DATA_WOF/whosonfirst-data-macroregion-latest -c macroregion_id -t 6
./rerun.sh -o $DATA_OUT/marinearea.csv    -d $DATA_WOF/whosonfirst-data-marinearea-latest -c marinearea_id -t 6
./rerun.sh -o $DATA_OUT/marketarea.csv    -d $DATA_WOF/whosonfirst-data-marketarea-latest -c marketarea_id -t 6
./rerun.sh -o $DATA_OUT/microhood.csv     -d $DATA_WOF/whosonfirst-data-microhood-latest -c microhood_id -t 6
./rerun.sh -o $DATA_OUT/neighbourhood.csv -d $DATA_WOF/whosonfirst-data-neighbourhood-latest -c neighbourhood_id -t 6
./rerun.sh -o $DATA_OUT/ocean.csv         -d $DATA_WOF/whosonfirst-data-ocean-latest -c ocean_id -t 6
./rerun.sh -o $DATA_OUT/planet.csv        -d $DATA_WOF/whosonfirst-data-planet-latest -c planet_id -t 6
./rerun.sh -o $DATA_OUT/region.csv        -d $DATA_WOF/whosonfirst-data-region-latest -c region_id -t 6
./rerun.sh -o $DATA_OUT/timezone.csv      -d $DATA_WOF/whosonfirst-data-timezone-latest -c timezone_id -t 6