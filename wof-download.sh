#!/bin/bash
DATA_WOF="./data/wof"
[ -d $DATA_WOF/compressed ] || mkdir -p $DATA_WOF/compressed

wget -i ./config/wof-data-links.dist.whosonfirst.org -P $DATA_WOF/compressed/

tar -xjf $DATA_WOF/compressed/whosonfirst-data-borough-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-campus-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-continent-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-country-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-county-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-dependency-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-disputed-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-empire-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-localadmin-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-locality-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-macrocounty-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-macrohood-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-macroregion-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-marinearea-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-marketarea-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-microhood-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-neighbourhood-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-ocean-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-planet-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-region-latest.tar.bz2 -C $DATA_WOF
tar -xjf $DATA_WOF/compressed/whosonfirst-data-timezone-latest.tar.bz2 -C $DATA_WOF