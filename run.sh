#!/bin/bash
# Exit key
key="q"

# Check virtual environment
[ ! -d ./venv ] && ./create-venv.sh

# Check data directory
[ ! -d ./data ] && mkdir ./data
[ ! -d ./data/log ] && mkdir ./data/log
[ ! -d ./data/error ] && mkdir ./data/error

# Activate virtual environment
source venv/bin/activate
# Install dependencies
pip -q install -r ./config/deps.list

# Run the program
python app.py $@ &
_PID=$!
# Track program's CPU and memory behaviors
psrecord $_PID --interval 2 --log "./data/log/ps-activity-`date +%F--%T`.log" --plot "./data/log/ps-activity-`date +%F--%T`.png" &

# Close the program with $key key
while read -n1 char ; do
 if [ "$char" = "$key" ] ; then
  kill "$_PID"
  break
 fi
done

# New line
echo ""

# Rerun program if it crashes
#while [ $? ]
#do
#  python app.py $@
#done