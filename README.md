# geohash-addresser
geohash-addresser is a tool to convert whosonfirst data into geohash identified CSV file.

## Logs
When program starts `data/log` directory appears. It contain `ps-activity<start_date>.log` and `ps-activity<start_date>.png` files.
### `ps-activity<start_date>.log`
This file contains CPU, shared Mem and Virtual Mem usages.
### `ps-activity<start_date>.png`
This file contains a plot of CPU and Mem usages.

## Usage
### Data download
#### link preparation
`lib/fill_wof_link.py` script prepares download links from https://dist.whosonfirst.org/bundles/ web site.
```bash
(venv)$ python lib/fill_wof_link.py > config/wof-data-links.dist.whosonfirst.org
```
#### Download & Extract
`./wof-download.sh` downloads the wof data into `data/wof/compressed` directory and extracts each of the `.tar.bz2` file into `data/wof` directory.
### Start
#### ES import
```bash
./run.sh -o <output_csv_file_name> -q <elastic_query_file_path> -c <(optional) check_id_column_name> -t <thread_num>
```
#### WOF file import
```bash
./run.sh -o <output_csv_file_name> -d <wof_data_directory_path> -c <(optional) check_id_column_name> -t <thread_num>
```
#### Parameters
* `-o`, `--output` determines the output file name. If the output file exists, program will append new values.
* If `-q` or `--query` parameter is given, ES import process will start. To start WOF file import `-d` or `--data` parameter should be given.
    * `-q`, `--query` is the path of the file that contains elastic query.
    * `-d`, `--data` is the path of Whos On First data directory
* `-c`, `--coulmn_name` is the column name to check that id written before. This parameter only meaningful with an existing `<output_csv_file_name>`.
* `-t`, `--thread_num` determines the thread number that program will run. Optional, default number is 4
### Exit
To end the program press `q`.
