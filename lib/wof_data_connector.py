import os
import re
import json

class WoF_data_reader:
    def __init__(
            self,
            data_directory
    ):
        self._wof_directory_root = data_directory
        self._meta_csv_path = None
        self._data_directory = None
        self._check_and_init_directory()

        self._walker = [self._data_directory]

    def _check_and_init_directory(self):
        print(os.getcwd())
        if os.path.isdir(self._wof_directory_root):
            meta_path = os.path.join(
                self._wof_directory_root,
                "meta"
            )
            if os.path.isdir(meta_path):
                meta_path_list = os.listdir(meta_path)
                if len(meta_path_list) == 1 and ".csv" in meta_path_list[0]:
                    self._meta_csv_path = os.path.join(
                        meta_path,
                        meta_path_list[0]
                    )

            self._data_directory = os.path.join(
                self._wof_directory_root,
                "data"
            )
            if not os.path.isdir(self._data_directory):
                raise NotADirectoryError("No \"data\" directory!")

    def _get_geojson_file(self):
        if len(self._walker) == 0:
            return None

        lookup = self._walker.pop()
        if os.path.isdir(lookup):
            dir_list = os.listdir(lookup)
            self._walker.extend(map(lambda x: os.path.join(lookup, x), dir_list))
            return self._get_geojson_file()
        elif os.path.isfile(lookup):
            if re.search("[0-9]+\\.geojson", lookup):
                return lookup
            else:
                return self._get_geojson_file()

    def _get_geojson(self):
        return json.load(
            open(
                self._get_geojson_file(),
                "r"
            )
        )

    def __iter__(self):
        return self

    def __next__(self):
        if not self._walker:
            raise StopIteration()

        return self._get_geojson()
