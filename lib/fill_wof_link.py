import requests
import re
from bs4 import BeautifulSoup

unwanted_list = [
    "planet",
    "timezone"
]

url = "https://dist.whosonfirst.org/bundles/"
web_site = requests.get(url)

soup = BeautifulSoup(web_site.text, 'html.parser')

entries = soup.findAll("li")

for entry in entries:
    strong = entry.find("strong")
    a = strong.find("a")
    if re.search("whosonfirst-data-[a-z]+-latest.tar.bz2", a.text):
        print(url + a["href"])