import json
from elasticsearch import Elasticsearch

class ES_connector:
    def __init__(
        self,
        host_config = "./config/elastic-hosts.json",
        query = "./config/whosonfirst-query.json",
        timeout = "500s",
        index = "poi_vlx",
        bunch_size = 100
    ):
        self.connection_config = json.load(
            open(
                host_config,
                "r"
            )
        )

        self.whosonfirst_query = json.load(
            open(
                query,
                "r"
            )
        )

        self.timeout = timeout
        self.index = index
        self.bunch_size = bunch_size

        self.connection = None
        self.old_scroll_id = None
        self.data_point_count = 0
        self.total_data_point = 0
    
    def is_it_connected(self):
        if self.connection is not None:
            if self.connection.ping() == True:
                return True
        return False

    def connect(self):
        if not self.is_it_connected():
            self.connection = Elasticsearch(
                hosts = self.connection_config,
                retry_on_timeout=True,
                max_retries=100,
                timeout=10000
            )

    def get_bunch(self):
        resp = None
        if self.old_scroll_id is None:
            resp = self.connection.search(
                index = self.index,
                body = self.whosonfirst_query,
                size = self.bunch_size,
                scroll = self.timeout
            )

            self.old_scroll_id = resp['_scroll_id']
        else:
            resp = self.connection.scroll(
                scroll_id = self.old_scroll_id,
                scroll = self.timeout
            )

            self.old_scroll_id = resp['_scroll_id']

        self.total_data_point = resp["hits"]["total"]
        self.data_point_count += len(resp["hits"]["hits"])
        return resp["hits"]["hits"]