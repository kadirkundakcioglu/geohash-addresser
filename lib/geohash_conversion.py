import queue
import threading
from queue import Empty
from copy import deepcopy
from concurrent.futures import ThreadPoolExecutor
from polygon_geohasher.polygon_geohasher import geohash_to_polygon

__base32 = '0123456789bcdefghjkmnpqrstuvwxyz'

lock = threading.RLock()

def sub_geohashes(geohash):
    for letter in __base32:
        yield geohash + letter

def splitter(q, inner, precision_limit, worker_stat_l, worker_num):
    # print("{} thread started".format(threading.current_thread().name))
    geohashes = []

    while sum(worker_stat_l) != 0:
        while not q.empty():
            try:
                current_geohash, polygon = q.get(block=False)
            except Empty:
                break

            if worker_stat_l[worker_num] == 0:
                lock.acquire()
                worker_stat_l[worker_num] = 1
                lock.release()
                # print(
                #     "[Working] w_s_l: {} - w_n: {} - Thread ID: {}".format(
                #         worker_stat_l,
                #         worker_num,
                #         threading.get_ident()
                #     )
                # )

            current_geohash_polygon = geohash_to_polygon(current_geohash)
            if polygon.contains(current_geohash_polygon):
                # print(
                #     "Added geohash:",
                #     current_geohash
                # )
                geohashes.append(current_geohash)
            elif polygon.intersects(current_geohash_polygon):
                # print(
                #     "Intersects geohash:",
                #     current_geohash
                # )
                if precision_limit <= len(current_geohash):
                    if not inner:
                        geohashes.append(current_geohash)
                else:
                    for i in sub_geohashes(current_geohash):
                        q.put((i, polygon.intersection(current_geohash_polygon)))
        if q.empty():
            if worker_stat_l[worker_num] == 1:
                lock.acquire()
                worker_stat_l[worker_num] = 0
                lock.release()
                # print(
                #     "[Waiting] w_s_l: {} - w_n: {} - Thread ID: {}".format(
                #         worker_stat_l,
                #         worker_num,
                #         threading.get_ident()
                #     )
                # )
    # print(
    #     "{} thread ended".format(
    #         threading.current_thread().name
    #     )
    # )
    return geohashes

def polygon_to_geohash(polygon, precision, inner = False, thread_num = 4):
    worker_count = thread_num
    worker_stat_l = [1 for i in range(worker_count)]
    polygon = polygon.buffer(0)

    q = queue.Queue()

    for i in __base32:
        q.put((i, deepcopy(polygon)))

    executor = ThreadPoolExecutor(
        max_workers=worker_count,
        thread_name_prefix="geohash-addresser"
    )

    results = []
    for i in range(worker_count):
        results.append(
            executor.submit(
                splitter,
                q,
                inner,
                precision,
                worker_stat_l,
                i
            )
        )

    geohashes = set()
    for result in results:
        geohashes = geohashes.union(
            result.result()
        )

    return geohashes
