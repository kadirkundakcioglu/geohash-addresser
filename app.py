import os
import csv
import geohash
import datetime
import argparse
from copy import deepcopy
from lib.geohash_conversion import polygon_to_geohash
from shapely import geometry
from lib.es_connect import ES_connector
from lib.wof_data_connector import WoF_data_reader
from lib.fieldlist import field_names
thread_num = 4
check_column_name = None

def get_geohashes(polygon_filed):
    if polygon_filed is None:
        return set()

    if polygon_filed["type"] == "Point":
        return {geohash.encode(
            polygon_filed["coordinates"][1],
            polygon_filed["coordinates"][0],
            7
        )}

    polygon = geometry.shape(polygon_filed)
    geohashes = polygon_to_geohash(
        polygon,
        7,
        inner=False,
        thread_num=thread_num
    )
    return geohashes

def get_parent_dict(parent_filed):
    if parent_filed is None:
        return {}

    keys = parent_filed.keys()
    result = {}
    for key in keys:
        result[key] = parent_filed[key][0]
    return result

def prepare_check_set(csv_file_name):
    if check_column_name is None:
        return set()

    csv_file = open(csv_file_name, "r")
    csv_reader = csv.DictReader(csv_file)

    check_set = set()
    for row in csv_reader:
        row_value = row.get(check_column_name)
        if row_value != "":
            check_set.add(row_value)

    csv_file.close()
    return check_set

def es_data_process(
    output_file_name,
    query_json
):
    es = ES_connector(
        query = query_json,
        timeout = "1d",
        bunch_size = 10
    )
    es.connect()

    check_set = prepare_check_set(output_file_name)
    csv_file = open(output_file_name, "a")
    csv_writer = csv.DictWriter(csv_file, fieldnames=field_names)

    # If CSV file exists, do not write header
    if check_column_name is None:
        csv_writer.writeheader()

    resp = es.get_bunch()
    progress_start_time = datetime.datetime.now()
    total_passed_data_point = 0
    print("Process Started")
    while len(resp):
        for data_point in resp:
            data_point = data_point["_source"]
            bunch_start_time = datetime.datetime.now()

            print(
                "WOF_ID:",
                data_point.get("source_id"),
                "\t|",
                "Name:",
                data_point.get("name").get("default")
            )

            filed_values = get_parent_dict(
                data_point.get("parent")
            )

            pass_flag = False
            if check_column_name is not None:
                if filed_values.get(check_column_name) in check_set:
                    total_passed_data_point += 1
                    pass_flag = True

            if not pass_flag:
                geohashes = get_geohashes(
                    data_point.get("polygon")
                )

                for geohash in geohashes:
                    c_filed_values = deepcopy(filed_values)
                    c_filed_values["geohash"] = geohash
                    csv_writer.writerow(c_filed_values)

            print(
                "\t- Time info:",
                bunch_start_time,
                "|",
                datetime.datetime.now() - bunch_start_time,
                "|",
                "Passed: ",
                pass_flag,
                "|",
                "ETC:",
                datetime.timedelta(
                    seconds=(
                        (datetime.datetime.now() - progress_start_time).seconds
                        * ((es.total_data_point - total_passed_data_point) /
                           ((es.data_point_count - total_passed_data_point) or 1))
                    )
                )
            )
        csv_file.flush()
        print(
            "Count:",
            es.data_point_count,
            "/",
            es.total_data_point
        )
        resp = es.get_bunch()
        print("New data bunch came")
    csv_file.close()

def wof_data_process(
    output_file_name,
    wof_data_directory
):
    wof_connector = WoF_data_reader(
        wof_data_directory
    )

    check_set = prepare_check_set(output_file_name)
    csv_file = None
    if check_column_name is None:
        csv_file = open(output_file_name, "w")
    else:
        csv_file = open(output_file_name, "a")
    csv_writer = csv.DictWriter(csv_file, fieldnames=field_names)

    # If CSV file exists, do not write header
    if check_column_name is None:
        csv_writer.writeheader()

    progress_start_time = datetime.datetime.now()
    total_passed_data_point = 0
    print("Process Started")
    for data_point in wof_connector:
        total_passed_data_point += 1
        start_time = datetime.datetime.now()

        field_data_temp = {
            "bbox": data_point["properties"].get("geom:bbox"),
            "latitude": data_point["properties"].get("geom:latitude"),
            "longitude": data_point["properties"].get("geom:longitude"),
            "iso": data_point["properties"].get("iso:country"),
            "wof_placetype": data_point["properties"].get("wof:placetype"),
            "name": data_point["properties"].get("wof:name"),
            "id": data_point["properties"].get("wof:id"),
            "hierarchy_num": 0,
            "parent_id": data_point["properties"].get("wof:parent_id"),
            "geonames_id": data_point["properties"].get("wof:concordances")
                           and data_point["properties"].get("wof:concordances").get("gn:id")
        }

        print(
            "WOF_ID:",
            field_data_temp.get("id"),
            "\t|",
            "No:",
            total_passed_data_point,
            "\t|",
            "Name:",
            field_data_temp.get("name")
        )

        hierarchy_data = data_point["properties"].get("wof:hierarchy") or []
        field_data = []
        for i, hierarchy in enumerate(hierarchy_data):
            temp_field_dict = deepcopy(field_data_temp)
            temp_field_dict["hierarchy_num"] = i
            for key, value in hierarchy.items():
                temp_field_dict[key] = value
            # Pass control
            if check_column_name is not None:
                if temp_field_dict.get(check_column_name) in check_set:
                    total_passed_data_point += 1
                    continue
            field_data.append(temp_field_dict)


        geohashes = get_geohashes(
            data_point.get("geometry")
        )

        for geohash in geohashes:
            for field_dict in field_data:
                c_filed_values = deepcopy(field_dict)
                c_filed_values["geohash"] = geohash
                csv_writer.writerow(c_filed_values)
        csv_file.flush()

        print(
            "\t- Time info:",
            start_time,
            "|",
            datetime.datetime.now() - start_time,
        )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", type=str, required=True)
    data_resource_group = parser.add_mutually_exclusive_group(required=True)
    data_resource_group.add_argument("-q", "--query", type=str)
    data_resource_group.add_argument("-d", "--data", type=str)
    parser.add_argument("-c", "--column_name", type=str, required=False)
    parser.add_argument("-t", "--thread_num", type=int, required=False, default=4)
    arguments = parser.parse_args()

    thread_num = arguments.thread_num
    if arguments.column_name is not None:
        # Check is there a CSV file
        if os.path.isfile(arguments.output):
            # If there is, init column name
            print("check column init!")
            check_column_name = arguments.column_name

    if arguments.query is not None:
        es_data_process(
            arguments.output,
            arguments.query
        )
    else:
        wof_data_process(
            arguments.output,
            arguments.data
        )
